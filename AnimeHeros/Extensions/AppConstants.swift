//
//  AppConstants.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/19/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import Foundation
import UIColor_Hex_Swift

class AppConstants {
    
    static let navigationBackgroundColor = UIColor("#FF0000")
    static let appPrimaryColor = UIColor("#FFFFFF")
    static let loadingBackground = "loading"
    static let subscribeButton = "Subscribe"
    static let subscribeTitle = "Subscribe"


}
