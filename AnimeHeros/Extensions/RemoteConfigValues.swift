//
//  RemoteConfigValues.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/20/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON

enum ValueKey: String {
    case appPrimaryColor
    case navigationBackgroundColor
    case loadingBackground
    case subscribeButton
    case subscribeTitle
}

class RemoteConfigValues {
    
    static let shared = RemoteConfigValues()
    var loadingDoneCallback: (() -> ())?
    var fetchComplete: Bool = false
    
    private init(){
        loadDefaultValues()
        fetchCloudeValues()
    }
    
    func string(forKey key: ValueKey) -> String {
        return RemoteConfig.remoteConfig()[key.rawValue].stringValue ?? ""
    }
    
    func loadingBackground(forKey key:ValueKey) -> UIImage {
        return UIImage(named: RemoteConfig.remoteConfig()[key.rawValue].stringValue ?? "loading")!
    }
    
    func color(forKey key: ValueKey) -> UIColor {
        return UIColor(RemoteConfig.remoteConfig()[key.rawValue].stringValue ?? "#FFFFFF")
    }
    
    func loadDefaultValues(){
        
        let appDefaults: [String: NSObject] = [
            ValueKey.appPrimaryColor.rawValue : "#FFFFFF" as NSObject,
            ValueKey.navigationBackgroundColor.rawValue : "#FF0000" as NSObject,
            ValueKey.loadingBackground.rawValue : "loading" as NSObject,
            ValueKey.subscribeButton.rawValue : "Subscribe" as NSObject,
            ValueKey.subscribeTitle.rawValue : "Subscribe" as NSObject

        ]
        
        RemoteConfig.remoteConfig().setDefaults(appDefaults)
    }
    
    func fetchCloudeValues(){
        let fetchDuration: TimeInterval = 0 // Note for me : in production it would take up to 12 hour to fetch the new values so i don't have to do this step in production phase (43200) every 12h we can change it as the client ask
        activateDebugMode()
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration, completionHandler: { (status, error) in
            
            guard error == nil else {
                print("an error fetching remote values \(error?.localizedDescription ?? "")")
                return
            }
            RemoteConfig.remoteConfig().activateFetched()
            print("Retrieved values from the cloud!")
            // let's test if the values come from the server
            print ("App primary color is \(RemoteConfig.remoteConfig().configValue(forKey: "appPrimaryColor").stringValue ?? "")") // return response is UTF8-encoded string
            print ("Navigation background color is \(RemoteConfig.remoteConfig().configValue(forKey: "navigationBackgroundColor").stringValue ?? "")") // .stringValue .boolValue we should cast it ;)
            print ("Country Loading Image \(RemoteConfig.remoteConfig().configValue(forKey: "loadingBackground").stringValue ?? "")") // check image for country
            print ("Subscribe Button \(RemoteConfig.remoteConfig().configValue(forKey: "subscribeButton").stringValue ?? "")") // A/B Testing
            print ("Subscribe Title \(RemoteConfig.remoteConfig().configValue(forKey: "subscribeTitle").stringValue ?? "")") // A/B Testing
            self.fetchComplete = true
            self.loadingDoneCallback?() // we check that the new values loaded sucess to the app
        })
    }
    
    // Since it's developer mode so we dont have to call multi request to fire base due to timeinterval so i should enable the debug mode ;)
    
    func activateDebugMode() {
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings!
    }
    
    // this would only used for 10 users or and do bypass for client side but in production we don't have to use it ;)
    
}
