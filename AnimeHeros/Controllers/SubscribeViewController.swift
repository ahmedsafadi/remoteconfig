//
//  SubscribeViewController.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/21/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import UIKit
import Firebase

class SubscribeViewController: UIViewController {
    
    @IBOutlet weak var but: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent("AnimeNewsletterPageLoaded", parameters: nil)
        self.title = RemoteConfigValues.shared.string(forKey: .subscribeTitle)
        self.but.setTitle(RemoteConfigValues.shared.string(forKey: .subscribeButton), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func subscribe(_ sender: UIButton) {
        Analytics.logEvent("AnimeNewsletterSubscribed", parameters: nil)
    }


}
