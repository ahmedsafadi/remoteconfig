//
//  HerosViewController.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/19/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
class HerosViewController: UIViewController {
    
    private let takenSurveyKey = "takenSurvey"
    
    @objc func runUserSurvey() {
        let alertController = UIAlertController(title: "User survey",
                                                message: "How do you feel about add anime trailer ?",
                                                preferredStyle: .actionSheet)
        
        let fanOfPluto = UIAlertAction(title: "Yes i want !", style: .default) { _ in
            Analytics.setUserProperty("true", forName: "wantTrailerAnime")
        }
        
        let notAFan = UIAlertAction(title: "Not worth to me", style: .default) { _ in
            Analytics.setUserProperty("false", forName: "wantTrailerAnime")
        }
        
        alertController.addAction(fanOfPluto)
        alertController.addAction(notAFan)
        navigationController?.present(alertController, animated: true)
        
        UserDefaults.standard.set(true, forKey: takenSurveyKey)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !UserDefaults.standard.bool(forKey: takenSurveyKey) {
            runUserSurvey()
        }
    }

    
    
    var Heros = [JSON]()
    
    func setupDemoData(){
        Heros.append(["image":"1","name":"Tokyo Ghoul","type":"Tv","episodes":"12","Air":"Dec 12, 2018"])
        Heros.append(["image":"2","name":"Attack On Titan","type":"Tv","episodes":"10","Air":"Apr 5, 2018"])
        Heros.append(["image":"3","name":"One Piece","type":"Tv","episodes":"20","Air":"Mar 3, 2018"])
        Heros.append(["image":"4","name":"One Punch Man 2","type":"Tv","episodes":"15","Air":"Jan 20, 2018"])
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigation()
        setupDemoData()
        Analytics.logEvent("mainPageLoaded", parameters: nil)
        
        let retakeSurveyButton = UIBarButtonItem(barButtonSystemItem: .compose,
                                                 target: self,
                                                 action: #selector(runUserSurvey))
        self.navigationItem.leftBarButtonItem = retakeSurveyButton
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeNavigation(){
        guard let navBar = navigationController?.navigationBar else { return }
        navBar.barTintColor =  RemoteConfigValues.shared.color(forKey: .navigationBackgroundColor)
        let targetFont = UIFont(name: "Arial", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
        navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : RemoteConfigValues.shared.color(forKey: .appPrimaryColor),NSAttributedStringKey.font : targetFont]
    }
    
}

extension HerosViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Heros.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hero", for: indexPath) as! AnimeHerosCollectionViewCell
        
        cell.animeImage.image = UIImage(named:Heros[indexPath.row]["image"].stringValue)
        cell.animeName.text = Heros[indexPath.row]["name"].stringValue
        cell.animeType.text = Heros[indexPath.row]["type"].stringValue
        cell.animeEps.text = Heros[indexPath.row]["episodes"].stringValue
        cell.animeAir.text = Heros[indexPath.row]["Air"].stringValue
        
        return cell
    }
    
    
}
