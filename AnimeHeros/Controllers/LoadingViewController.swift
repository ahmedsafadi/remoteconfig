//
//  LoadingViewController.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/20/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import UIKit
import Firebase

class LoadingViewController: UIViewController {

    @IBOutlet weak var backGround: UIImageView!
    
    func startApp() {
        performSegue(withIdentifier: "loaded", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent("loadingPage", parameters: nil)

        backGround.image = RemoteConfigValues.shared.loadingBackground(forKey: .loadingBackground)

        if RemoteConfigValues.shared.fetchComplete {
            startApp()
        }
        
        RemoteConfigValues.shared.loadingDoneCallback = startApp
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
