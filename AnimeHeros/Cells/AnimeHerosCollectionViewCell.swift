//
//  AnimeHerosCollectionViewCell.swift
//  AnimeHeros
//
//  Created by Ahmed Safadi on 3/20/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import UIKit

class AnimeHerosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var animeImage: UIImageView!
    
    @IBOutlet weak var animeName: UILabel!
    
    @IBOutlet weak var animeType: UILabel!
    
    @IBOutlet weak var animeAir: UILabel!
    
    @IBOutlet weak var animeEps: UILabel!
    
}
